package org.gcube.vremanagement;


import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * 
 */
class CsvUpdateTest {

	private static Logger logger = LoggerFactory
			.getLogger(CsvUpdateTest.class);

	private String[] listVOs= {"D4Research","gCubeApps","FARM","ParthenosVO","OpenAIRE"};
	private String scopeIn = "/d4science.research-infrastructures.eu";
	private int skipLines = 3;
	private String InCsvFileName="VREDecommisioned-240326.csv";
	//Used for Google APIs remote update
	private String vreSheet = "VREs";
	private String missVreSheet="VREs-missing";
	private String updateVreSheet = "VREs-extended-info";
	//Used for GooGle APis;
	private String spreadID="1-_24RYwFHG5DuR-rrcmHDSz9pOqVHApBLhXiFLub9JA";
	
	private String filePath = "src/test/resources";
	
	@Test 
	void QueryClientVREInfoTest(){
		
		try(VREQueryService updaterCSV = new VREQueryService.VQSBuilder().setCSVFiles(filePath,InCsvFileName,skipLines).build())
		{
		String voIn="D4Research";
		String vreName="FoodborneOutbreak";
		logger.debug("Checking information about the VRE "+vreName+" in the VO:"+voIn);
		updaterCSV.getVREInfoFromClient(scopeIn,voIn, vreName);
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	void UpdateLocalCSVTest() {
		try(VREQueryService updaterCSV = new VREQueryService.VQSBuilder().setCSVFiles(filePath,InCsvFileName,skipLines).build())
		{
		logger.debug("Updating VREs in a LOCAL CSV file");
		updaterCSV.updateCSVformCSVList(scopeIn);
		logger.debug("Update CSV done");
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	void MissingLocalCSVTest(){
		try(VREQueryService updaterCSV = new VREQueryService.VQSBuilder().setCSVFiles(filePath,InCsvFileName,skipLines).build())
		{
		logger.debug("Getting missing VRE in a LOCAL CSV file");
		updaterCSV.missingVREinCSV(scopeIn, listVOs);
		logger.debug("Missing CSV done");
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	
	@Disabled
	@Test 
	void UpdateEndTimeVRETest(){
		try(VREQueryService updaterCSV = new VREQueryService.VQSBuilder().setCSVFiles(filePath,InCsvFileName,skipLines).build())
		{
		String voTimeIn="D4Research";
		String vreTimeName="FoodborneOutbreak";
		int yearIn=2024;
		int monthIn=2; //Months start from zero
		int dayIn=21;
		
		updaterCSV.updateEndTimeVRE(scopeIn, voTimeIn, vreTimeName, yearIn,monthIn, dayIn);
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	//Google remote APIs requires client_secret token
	@Disabled
	@Test
	
	void UpdateRemoteVREInfoSheet()
	{
		try {
		GoogleSheetsService.setup(spreadID);
		GoogleSheetsService.updateVresInfoSheet(vreSheet, updateVreSheet, "A1:G", scopeIn);
		}catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	@Disabled
	@Test
	
	void UpdateRemoteVREMissingSheet()
	{
		try {
			GoogleSheetsService.setup(spreadID);
			GoogleSheetsService.updateMissVREsGoogleSheet(scopeIn, listVOs, vreSheet, missVreSheet, "A1:Z");
		}catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	@Disabled
	@Test
	void InteractiveTest() {
		
		
		Scanner scanner = new Scanner(System.in);
		
		
		int choice=0;
		
		do {
			try(VREQueryService updaterCSV = new VREQueryService.VQSBuilder().setCSVFiles(filePath,InCsvFileName,skipLines).build())
			{
			
			logger.debug("Menu:");
			logger.debug("1. Update VREs in a LOCAL CSV file");
			logger.debug("2. Get missing VRE in a LOCAL CSV file");
			logger.debug("3. Get VRE info from Service");
			logger.debug("4. Update VRE EndTime");
			logger.debug("5. Update VRES in a Remore Google Sheet");
			logger.debug("6. Update Missing VRES in a Remore Google Sheet");
			logger.debug("7. Exit");
			logger.debug("Choice:");
			
			choice = scanner.nextInt();
			scanner.nextLine();
			
			switch (choice) {
				case 1:
					logger.debug("Updating VREs in a LOCAL CSV file");
					updaterCSV.updateCSVformCSVList(scopeIn);
					logger.debug("Update CSV done");
					break;
				case 2:
					logger.debug("Getting missing VRE in a LOCAL CSV file");
					updaterCSV.missingVREinCSV(scopeIn, listVOs);
					logger.debug("Missing CSV done");
					break;
				case 3:
					logger.debug("Getting VRE info from Service");
					logger.debug("Insert VRE name (ignore case): ");
					String vreName = scanner.nextLine();
					logger.debug("Insert VO: ");
					String voIn = scanner.nextLine();
					updaterCSV.getVREInfoFromClient(scopeIn,voIn, vreName);
					break;
				case 4: 
					logger.debug("Updating VRE EndTime");
					logger.debug("Insert VRE name (ignore case): ");
					String vreTimeName = scanner.nextLine();
					logger.debug("Insert VO: ");
					String voTimeIn = scanner.nextLine();
					if (updaterCSV.getVREInfoFromClient(scopeIn, voTimeIn, vreTimeName) >0)
					{
						logger.debug("Update year? (0=no, 1=yes)");
						if (scanner.nextInt()>0)
						{
							logger.debug("Insert year: ");
							int yearIn = scanner.nextInt();
							logger.debug("Insert month num: ");
							int monthIn = scanner.nextInt()-1;
							logger.debug("Insert day: ");
							int dayIn = scanner.nextInt();
							updaterCSV.updateEndTimeVRE(scopeIn, voTimeIn, vreTimeName, yearIn,monthIn, dayIn);
						}
					}
					break;
	
				case 5: 
					GoogleSheetsService.setup(spreadID);
					GoogleSheetsService.updateVresInfoSheet(vreSheet, updateVreSheet, "A1:G", scopeIn);

					break;
				case 6: 
					GoogleSheetsService.setup(spreadID);
					GoogleSheetsService.updateMissVREsGoogleSheet(scopeIn, listVOs, vreSheet, missVreSheet, "A1:Z");

					break;
				case 7:
					logger.debug("Exiting...");
					break;
				default:
					logger.debug("Invalid choice - Try Again");
					break;
			}
			}catch(InputMismatchException ime)
			{
				System.err.println("Input mismatch! Please use correct input type");
				scanner.nextLine();
				choice=0;
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}while(choice !=7); //end do-while loop
		scanner.close();
	}

}
