package org.gcube.vremanagement;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
	
	private static Logger logger = LoggerFactory
			.getLogger(App.class);

	private static String[] listVOs= {"D4Research","gCubeApps","FARM","ParthenosVO","OpenAIRE"};
	private static String scopeIn = "/d4science.research-infrastructures.eu";
	private static int skipLines = 3;
	private static String InCsvFileName="VREDecommisioned-240326.csv";
	private static String filePath="src/main/resources";
	//Used for GooGle APis;
	
	
	public static void main (String[] args)
	{
		
		try(VREQueryService updaterCSV = new VREQueryService.VQSBuilder().setCSVFiles(filePath,InCsvFileName,skipLines).build())
		{
			String voIn="D4Research";
			String vreName="FoodborneOutbreak";
			updaterCSV.getVREInfoFromClient(scopeIn,voIn, vreName);
			
			logger.debug("Updating VREs in a LOCAL CSV file");
			updaterCSV.updateCSVformCSVList(scopeIn);
			logger.debug("Update CSV done");
			
			logger.debug("Getting missing VRE in a LOCAL CSV file");
			updaterCSV.missingVREinCSV(scopeIn, listVOs);
			logger.debug("Missing CSV done");
			
		}catch (IOException e)
		{e.printStackTrace();};
		
	}
}