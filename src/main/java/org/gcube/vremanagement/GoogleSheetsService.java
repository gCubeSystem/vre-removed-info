package org.gcube.vremanagement;

import static org.gcube.vremanagement.vremodel.cl.plugin.AbstractPlugin.factory;

import static org.gcube.vremanagement.vremodel.cl.plugin.AbstractPlugin.manager;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.gcube.vremanagement.vremodel.cl.stubs.types.Report;
import org.gcube.vremanagement.vremodel.cl.stubs.types.VREDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AddSheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.ValueRange;


public class GoogleSheetsService {
    private static Sheets sheetsService;
    private static String SPREADSHEET_ID;
    private static SimpleDateFormat dateFormat;
    private static VREQueryService vreService;
    
    private static final Logger logger = LoggerFactory.getLogger(GoogleSheetsService.class);

    public static void setup(String _spreadID) throws GeneralSecurityException, IOException {
        sheetsService = SheetsServiceUtil.getSheetsService();
        vreService = VREQueryService.getInstance();
        dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mm a");
        SPREADSHEET_ID=_spreadID;
    }
    
    
    
    private static void createAndWriteToNewSheet(String sheetName)
            throws IOException {
        // Create the new sheet
    	
    	boolean sheetVREsExists = GoogleSheetsService.isSheetExists(sheetName);
    	
    	if(sheetVREsExists)
    	{
    		GoogleSheetsService.clearSheet(sheetName);
    		logger.debug("Sheet "+sheetName+"cleared");
    		
    	}
    	else { //create a new sheet    	
    		SheetProperties properties = new SheetProperties();
    		properties.setTitle(sheetName);
    		AddSheetRequest addSheetRequest = new AddSheetRequest().setProperties(properties);
    		BatchUpdateSpreadsheetRequest batchUpdateRequest = new BatchUpdateSpreadsheetRequest().setRequests(
            Arrays.asList(new Request().setAddSheet(addSheetRequest)));
        sheetsService.spreadsheets().batchUpdate(SPREADSHEET_ID, batchUpdateRequest).execute();

    		}
      
    }
    
    
    private static boolean isSheetExists(String _sheetName) throws IOException
    {
    	
    	Spreadsheet spreadsheet = sheetsService.spreadsheets().get(SPREADSHEET_ID).execute();
        List<Sheet> sheets = spreadsheet.getSheets();
        for (Sheet sheet : sheets) {
            if (sheet.getProperties().getTitle().equals(_sheetName)) {
                return true;
            }
        }
        return false;
    }
    
    
    private static void clearSheet(String _sheetName) throws IOException {
        sheetsService.spreadsheets().values()
                .clear(SPREADSHEET_ID, _sheetName, null)
                .execute();
    }
    
    
    //cell range A1:Z
    public static List<List<Object>> getRange(String _sheetName, String _cellRange) throws IOException, GeneralSecurityException
    {
    	if (sheetsService != null || vreService != null)
    	{
    	
    		String sheetRange = _sheetName+"!"+_cellRange;
    	
    		ValueRange response = sheetsService.spreadsheets().values().get(SPREADSHEET_ID, sheetRange).execute();
    	    	
    		List<List<Object>> values = response.getValues();
    		if ((values == null ) || values.isEmpty())
    		{
    			logger.debug("No data found");
    			return null;
    		}else {
    			return values;
    		}
    		//stem.out.println("ReadDone");
    	
    	}
    	return null;
   
    }
    
    
  //cell range A1:Z
    public static void updateVresInfoSheet(String _sheetVreName, String _sheetUpdateName,String _cellRange, String _scope) throws IOException, GeneralSecurityException
    {
    	
    	if (sheetsService != null || vreService != null)
    	{
    		String sheetRange = _sheetVreName+"!"+_cellRange;
    		int vreCount =0;
    		logger.debug("Sheet range: "+sheetRange);
    		ValueRange response = sheetsService.spreadsheets().values().get(SPREADSHEET_ID, sheetRange).execute();
    	    //getting values from the CSV google doc file
    		List<List<Object>> values = response.getValues();
    		List<List<Object>> updatedROWs = new ArrayList<>();
    		
    		updatedROWs.add(Arrays.asList("TICKET","STATUS", "SUBJECT", "VREName", "VO", "Gateway", "TICKET CLOSED", "START TIME","END TIME", "MANAGERS","DESCRIPTION"));
     		if ((values == null ) || values.isEmpty())
    		{
    			logger.debug("No data found");
    		}else {
    		    
    			for (List<Object> row : values)
    			{
    				if(!(row.size()<5))
    				{
    					try {
    					vreCount++;
    					//logger.debug("ROW:" +row +"ROW SIZE:" +row.size());
    					logger.debug("Updating VRENAME:"+row.get(3)+ "in the VO:" +row.get(4));
    					VREDescription vreFound = vreService.findVREDescr(_scope, row.get(4).toString(), row.get(3).toString());
    					
    					if(vreFound != null)
    					{
    						logger.debug("VRE NAME:" +vreFound.name());

        					row.addAll(Arrays.asList(dateFormat.format(vreFound.startTime().getTime()),
        							dateFormat.format(vreFound.endTime().getTime()),
        							vreFound.manager(),
        							vreFound.description()
        							));
        					
        					//batch update
        					updatedROWs.add(row);			
        					
    					}else
    					{
    						logger.debug("VRE "+row.get(3)+" in the VO "+row.get(4)+" NOT FOUND");
    						updatedROWs.add(row);
    					}
    					}catch (Exception e)
    					{
    						System.err.println("A map for "+_scope+"/"+row.get(4)+" is undefined");
    						e.printStackTrace();
    					}
    					
    				}
    				
    				
    			}
    			if(updatedROWs !=null)
    			{
    				GoogleSheetsService.updateVREsGoogleSheet(_sheetUpdateName, updatedROWs);
    				logger.debug("No VREs FOUND!");
    			}
    		}
    		logger.debug("ReadDone of "+vreCount +"VREs");
    	
    	}
    	
   
    }
    
    
    public static void updateVREsGoogleSheet(String sheetName, List<List<Object>> _batchRows) throws IOException
    {
    	GoogleSheetsService.createAndWriteToNewSheet(sheetName);
    	String writeRange = sheetName+"!A1:Z"+(_batchRows.size()+1);
    	ValueRange body = new ValueRange().setValues(_batchRows);
    	
    	
    	BatchUpdateValuesRequest batchUpdate = new BatchUpdateValuesRequest().setValueInputOption("RAW").setData(Collections.singletonList(body));
    	
    	// Write data to new sheet
        sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID, writeRange, body)
                .setValueInputOption("RAW")
                .execute();
        logger.debug("Row written to " + sheetName);
    	
    }
    
    
    public static boolean isVREinCSVGoogleSheet(List<List<Object>> _listRows, String _vo, String _vreName)
    {
    	
    	if(_listRows == null || _listRows.isEmpty())
    	{
    		System.err.println("No Data Found");
    		return false;
    	}
    	
    	for (List<Object> row : _listRows)
    	{
    		if (row.size()>5 && row.get(3) !=null && row.get(4)!= null)
    		{
    			if (row.get(3).toString().equalsIgnoreCase(_vreName) && row.get(4).toString().equalsIgnoreCase(_vo))
    				return true;
    		}
    	}
    	
    	logger.debug("VRE "+_vreName+" in the VO: "+_vo+" missing in the CSV!");
    	return false;
    	
   
    }
    
    
    //cellRange format "start_columnStartRow:EndColumn  e.g., A1:Z"
    public static void updateMissVREsGoogleSheet (String _scope, String[] _voList, String _sheetVREName, String _sheetMissName, String _cellRange) throws IOException, GeneralSecurityException
    {
    	//GoogleSheetsService.createAndWriteToNewSheet(_sheetMissName);
    	List<List<Object>> rowMissVre = new ArrayList<>();
    	rowMissVre.add(Arrays.asList("TICKET","STATUS", "SUBJECT", "VREName", "VO", "Gateway", "TICKET CLOSED", "START TIME","END TIME", "MANAGERS","DESCRIPTION"));
    	for (String voItem : _voList)
		{
			String scopeVO = _scope.concat("/").concat(voItem);
			List<Report> reportVREs = vreService.getReportVREs(scopeVO);
			List<List<Object>> listRowCSV = GoogleSheetsService.getRange(_sheetVREName, _cellRange);
			
 			for (Report report: reportVREs) 
			{
				W3CEndpointReference epr = factory().build().getEPRbyId(report.id());
				VREDescription vre= manager().at(epr).build().getDescription();
				if (report.state().compareTo("Disposed") ==0)
				{
					
					
					if(!GoogleSheetsService.isVREinCSVGoogleSheet(listRowCSV, voItem, vre.name()))
					{
						logger.debug("Updating remote missing VRE sheet with VRE "+vre.name());
						
						
						

				        rowMissVre.add(Arrays.asList(" ", 
				        		"Disposed", 
				        		scopeVO, 
				        		vre.name(), 
				        		voItem, 
				        		" ", 
				        		" ",
				        		dateFormat.format(vre.startTime().getTime()), 
								dateFormat.format(vre.endTime().getTime()), 
				        		vre.manager(),
				        		vre.description()
				        		));

				    
	
					}
					
					
			
				}
			}
 			
		}
    	GoogleSheetsService.updateVREsGoogleSheet(_sheetMissName, rowMissVre);
	
	}
    
}