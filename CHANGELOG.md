This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for vre-removed-info

## [v1.0.0-SNAPSHOT] 
- First commit with project structure
